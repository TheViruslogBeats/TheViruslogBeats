<h1 align="center">Привет! 👋, меня зовут Егор</h1>
<h3 align="center">Я фронтенд-разработчик</h3>

- 👨‍💻 Все мои проекты можно найти здесь: [https://github.com/TheViruslogBeats](https://github.com/TheViruslogBeats)

- 📫 Моя почта: **theviruslogbeats@gmail.com**

- 📄 Мой сайт-резюме: [https://theviruslogbeats.github.io/My-CV/](https://theviruslogbeats.github.io/My-CV/)

- ⚡ Смешной факт: **Программист - Битмейкер**

[![Anurag's GitHub stats](https://github-readme-stats.vercel.app/api?username=TheViruslogBeats&theme=react&show_icons=true)](https://github.com/anuraghazra/github-readme-stats)
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=TheViruslogBeats&theme=react&show_icons=true)](https://github.com/anuraghazra/github-readme-stats)
